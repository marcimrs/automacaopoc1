package br.com.core.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverHandler {
    public static WebDriver getDriver()
    {
        System.setProperty("webdriver.chrome.driver", "C:\\AutomacaoPOC\\src\\test\\resources\\mobile\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        return driver;
    }
}
